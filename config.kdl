pane_frames false
theme "catppuccin-mocha"
copy_on_select false
default_layout "default"
copy_command "xclip -selection clipboard"

ui {
    pane_frames {
        hide_session_name false
    }
}

keybinds clear-defaults=true{
    normal {
        bind "Alt c" { Copy; }
    }
    locked {
        bind "Alt g" { SwitchToMode "Normal"; }
    }
    resize {
        bind "Alt r" { SwitchToMode "Normal"; }
        bind "h" "Left" { Resize "Increase Left"; }
        bind "j" "Down" { Resize "Increase Down"; }
        bind "k" "Up" { Resize "Increase Up"; }
        bind "l" "Right" { Resize "Increase Right"; }
        bind "H" { Resize "Decrease Left"; }
        bind "J" { Resize "Decrease Down"; }
        bind "K" { Resize "Decrease Up"; }
        bind "L" { Resize "Decrease Right"; }
        bind "=" "+" { Resize "Increase"; }
        bind "-" { Resize "Decrease"; }
    }
    pane {
        bind "Alt w" { SwitchToMode "Normal"; }
        bind "h" "Left" { MoveFocus "Left"; }
        bind "l" "Right" { MoveFocus "Right"; }
        bind "j" "Down" { MoveFocus "Down"; }
        bind "k" "Up" { MoveFocus "Up"; }
        bind "H" { MovePane "Left"; }
        bind "J" { MovePane "Down"; }
        bind "K" { MovePane "Up"; }
        bind "L" { MovePane "Right"; }
        bind "Ctrl h" { NewPane "Left"; SwitchToMode "Normal"; }
        bind "Ctrl j" { NewPane "Down"; SwitchToMode "Normal"; }
        bind "Ctrl k" { NewPane "Up"; SwitchToMode "Normal"; }
        bind "Ctrl l" { NewPane "Right"; SwitchToMode "Normal"; }
        bind "n" "a" { NewPane; SwitchToMode "Normal"; }
        bind "Ctrl p" { MovePaneBackwards; }
        bind "Ctrl n" { MovePane; }
        bind "q" { CloseFocus; SwitchToMode "Normal"; }
        bind "F" { ToggleFocusFullscreen; SwitchToMode "Normal"; }
        bind "z" { TogglePaneFrames; SwitchToMode "Normal"; }
        bind "e" { ToggleFloatingPanes; SwitchToMode "Normal"; }
        bind "f" { TogglePaneEmbedOrFloating; SwitchToMode "Normal"; }
        bind "r" { SwitchToMode "RenamePane"; PaneNameInput 0;}
    }
    tab {
        bind "Alt t" { SwitchToMode "Normal"; }
        bind "Ctrl t" { SwitchToMode "Normal"; }
        bind "r" { SwitchToMode "RenameTab"; TabNameInput 0; }
        bind "h" "Left" "Up" "k" { GoToPreviousTab; }
        bind "l" "Right" "Down" "j" { GoToNextTab; }
        bind "H" "K" { MoveTab "left"; }
        bind "L" "J" { MoveTab "right"; }
        bind "n" "a" { NewTab; SwitchToMode "Normal"; }
        bind "x" { CloseTab; SwitchToMode "Normal"; }
        bind "s" { ToggleActiveSyncTab; SwitchToMode "Normal"; }
        bind "b" { BreakPane; SwitchToMode "Normal"; }
        bind "]" { BreakPaneRight; SwitchToMode "Normal"; }
        bind "[" { BreakPaneLeft; SwitchToMode "Normal"; }
        bind "1" { GoToTab 1; }
        bind "2" { GoToTab 2; }
        bind "3" { GoToTab 3; }
        bind "4" { GoToTab 4; }
        bind "5" { GoToTab 5; }
        bind "6" { GoToTab 6; }
        bind "7" { GoToTab 7; }
        bind "8" { GoToTab 8; }
        bind "9" { GoToTab 9; }
        bind "Tab" { ToggleTab; }
    }
    scroll {
        bind "Alt /" { SwitchToMode "Normal"; }
        bind "e" { EditScrollback; SwitchToMode "Normal"; }
        bind "s" { SwitchToMode "EnterSearch"; SearchInput 0; }
        bind "Ctrl c" { ScrollToBottom; SwitchToMode "Normal"; }
        bind "j" "Down" { ScrollDown; }
        bind "k" "Up" { ScrollUp; }
        bind "f" "PageDown" "Right" "l" { PageScrollDown; }
        bind "b" "PageUp" "Left" "h" { PageScrollUp; }
        bind "d" { HalfPageScrollDown; }
        bind "u" { HalfPageScrollUp; }
        bind "c" { Copy; }
    }
    search {
        bind "Alt /" { SwitchToMode "Normal"; }
        bind "Ctrl c" { ScrollToBottom; SwitchToMode "Normal"; }
        bind "s" { SwitchToMode "EnterSearch"; SearchInput 0; }
        bind "j" "Down" { ScrollDown; }
        bind "k" "Up" { ScrollUp; }
        bind "f" "PageDown" "Right" "l" { PageScrollDown; }
        bind "b" "PageUp" "Left" "h" { PageScrollUp; }
        bind "d" { HalfPageScrollDown; }
        bind "u" { HalfPageScrollUp; }
        bind "n" { Search "down"; }
        bind "N" "p" { Search "up"; }
        bind "i" { SearchToggleOption "CaseSensitivity"; }
        bind "w" { SearchToggleOption "Wrap"; }
        bind "o" { SearchToggleOption "WholeWord"; }
        bind "c" { Copy; }
    }
    entersearch {
        bind "Ctrl c" "Esc" { SwitchToMode "Scroll"; }
        bind "Enter" { SwitchToMode "Search"; }
    }
    renametab {
        bind "Ctrl c" { SwitchToMode "Normal"; }
        bind "Esc" { UndoRenameTab; SwitchToMode "Tab"; }
    }
    renamepane {
        bind "Ctrl c" { SwitchToMode "Normal"; }
        bind "Esc" { UndoRenamePane; SwitchToMode "Pane"; }
    }
    session {
        bind "Alt l" { SwitchToMode "Normal"; }
        bind "d" "Alt d" { Detach; }
        bind "Alt q" { Quit; }
        bind "w" {
            LaunchOrFocusPlugin "zellij:session-manager" {
                floating true
                move_to_focused_tab true
            };
            SwitchToMode "Normal"
        }
        bind "p" {
            LaunchOrFocusPlugin "zellij:plugin-manager" {
                floating true
                move_to_focused_tab true
            };
            SwitchToMode "Normal"
        }
    }
    shared_except "locked" {
        bind "Alt g" { SwitchToMode "Locked"; }
        bind "Alt n" { NewPane; }
        bind "Alt h" { MoveFocus "Left"; }
        bind "Alt l" { MoveFocus "Right"; }
        bind "Alt j" { MoveFocus "Down"; }
        bind "Alt k" { MoveFocus "Up"; }
        // bind "Alt H" { MovePane "Left"; }
        // bind "Alt J" { MovePane "Down"; }
        // bind "Alt K" { MovePane "Up"; }
        // bind "Alt L" { MovePane "Right"; }
        bind "Alt E" { ToggleFloatingPanes; SwitchToMode "Normal"; }
        bind "Alt G" { TogglePaneEmbedOrFloating; SwitchToMode "Normal"; }
        bind "Alt F" { ToggleFocusFullscreen; SwitchToMode "Normal"; }
		bind "Alt Right" { GoToNextTab; }
		bind "Alt Left" { GoToPreviousTab; }
        bind "Alt [" { PreviousSwapLayout; }
        bind "Alt ]" { NextSwapLayout; }
        bind "Alt 1" { GoToTab 1; }
        bind "Alt 2" { GoToTab 2; }
        bind "Alt 3" { GoToTab 3; }
        bind "Alt 4" { GoToTab 4; }
        bind "Alt 5" { GoToTab 5; }
        bind "Alt 6" { GoToTab 6; }
        bind "Alt 7" { GoToTab 7; }
        bind "Alt 8" { GoToTab 8; }
        bind "Alt 9" { GoToTab 9; }
    }
    shared_except "normal" "locked" {
        bind "Enter" "Esc" { SwitchToMode "Normal"; }
    }
    shared_except "pane" "locked" {
        bind "Alt w" { SwitchToMode "Pane"; }
    }
    shared_except "resize" "locked" {
        bind "Alt r" { SwitchToMode "Resize"; }
    }
    shared_except "scroll" "locked" {
        bind "Alt /" { SwitchToMode "Scroll"; }
    }
    shared_except "session" "locked" {
        bind "Alt L" { SwitchToMode "Session"; }
    }
    shared_except "tab" "locked" {
        bind "Alt t" { SwitchToMode "Tab"; }
    }
}

plugins {
    tab-bar { path "tab-bar"; }
    status-bar { path "status-bar"; }
    strider { path "strider"; }
    compact-bar { path "compact-bar"; }
    session-manager { path "session-manager"; }
    plugin-manager { path "plugin-manager"; }
}
