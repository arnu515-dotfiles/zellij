# Keybinds cheat sheet

## Locked mode

| Key | Function |
| :-: | :------: |
| Alt g | Toggle lock mode |

## Resize mode

| Key | Function |
| :-: | :------: |
| Alt r | Toggle resize mode |
